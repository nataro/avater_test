#ifndef EFFECT_H_
#define EFFECT_H_

#include <utility/In_eSPI.h>
#include "DrawContext.h"
#include "Drawable.h"

namespace m5_test{

class Effect final : public Drawable{
    private:
        void DrawBubbleMark(TFT_eSPI *spi, uint32_t x, uint32_t y, uint32_t radius, 
                            uint32_t color){
            DrawBubbleMark(spi, x, y, radius, color, 0);
        }
        void DrawBubbleMark(TFT_eSPI *spi, uint32_t x, uint32_t y, uint32_t radius, 
                            uint32_t color, float_t offset){
           radius = radius + floor(radius * 0.2 * offset);
           spi->drawCircle(x, y, radius, color);
           spi->drawCircle(x - (radius/4), y- (radius/4), radius/4, color);
        }

        void DrawSweatMark(TFT_eSPI *spi, uint32_t x, uint32_t y, uint32_t radius, 
                            uint32_t color){
            DrawSweatMark(spi, x, y, radius, color, 0);
        }
        void DrawSweatMark(TFT_eSPI *spi, uint32_t x, uint32_t y, uint32_t radius, 
                            uint32_t color, float_t offset){
            y = y + floor(5 * offset);
            radius = radius +floor(radius * 0.2 * offset);
            spi->fillCircle(x, y, radius, color);
            uint32_t a = (sqrt(3) * radius) /2;
            spi->fillTriangle(x, y - radius * 2, x - a, y - radius * 0.5, x + a, y - radius * 0.5, color);
        }

        void DrawChillMark(TFT_eSPI *spi, uint32_t x, uint32_t y, uint32_t radius,
                            uint32_t color) {
            DrawChillMark(spi, x, y, radius, color, 0);
        }
        void DrawChillMark(TFT_eSPI *spi, uint32_t x, uint32_t y, uint32_t radius, 
                            uint32_t color, float_t offset) {
            uint32_t h = radius + abs(radius * 0.2 * offset);
            spi->fillRect(x - (radius / 2), y, 3, h / 2, color);
            spi->fillRect(x, y, 3, h * 3 / 4, color);
            spi->fillRect(x + (radius / 2), y, 3, h, color);
        }

        void DrawAngerMark(TFT_eSPI *spi, uint32_t x, uint32_t y, uint32_t radius, 
                            uint32_t color, uint32_t bColor) {
            DrawAngerMark(spi, x, y, radius, color, bColor, 0);
        }

        void DrawAngerMark(TFT_eSPI *spi, uint32_t x, uint32_t y, uint32_t radius, 
                            uint32_t color, uint32_t bColor, float_t offset) {
            radius = radius + abs(radius * 0.4 * offset);
            spi->fillRect(x - (radius / 3), y - radius, (radius * 2) / 3, radius * 2, color);
            spi->fillRect(x - radius, y - (radius / 3), radius * 2, (radius * 2) / 3, color);
            spi->fillRect(x - (radius / 3) + 2, y - radius, ((radius * 2) / 3) - 4, radius * 2, bColor);
            spi->fillRect(x - radius, y - (radius / 3) + 2, radius * 2, ((radius * 2) / 3) - 4, bColor);
        }

        void DrawHeartMark(TFT_eSPI *spi, uint32_t x, uint32_t y, uint32_t radius,
                        uint32_t color) {
            DrawHeartMark(spi, x, y, radius, color, 0);
        }

        void DrawHeartMark(TFT_eSPI *spi, uint32_t x, uint32_t y, uint32_t radius,
                        uint32_t color, float_t offset) {
            radius = radius + floor(radius * 0.4 * offset);
            spi->fillCircle(x - radius / 2, y, radius / 2, color);
            spi->fillCircle(x + radius / 2, y, radius / 2, color);
            float_t a = (sqrt(2) * radius) / 4.0;
            spi->fillTriangle(x, y, x - radius / 2 - a, y + a, x + radius / 2 + a, y + a, color);
            spi->fillTriangle(x, y + (radius / 2) + 2 * a, x - radius / 2 - a, y + a,
                            x + radius / 2 + a, y + a, color);
        }

        public:
            Effect() = default;
            ~Effect() = default;
            Effect(const Effect &other) = default;
            Effect &operator=(const Effect &other) = default;

            void draw(TFT_eSPI *spi, BoundingRect rect, DrawContext *ctx) override {
                uint32_t primary_color = ctx->palette()->Get(COLOR_PRIMARY);
                uint32_t bg_color = ctx->palette()->Get(COLOR_BACKGROUND);
                float_t offset = ctx->breath();
                Expression exp = ctx->expression();
                switch(exp){
                    case Expression::Doubt:
                        DrawSweatMark(spi, 290, 110, 7, primary_color, -offset);
                        break;
                    case Expression::Angry:
                        DrawAngerMark(spi, 280, 50, 12, primary_color, bg_color, offset);
                        break;
                    case Expression::Happy:
                        DrawHeartMark(spi, 280, 50, 12, primary_color, offset);
                        break;
                    case Expression::Sad:
                        DrawChillMark(spi, 270, 0, 30, primary_color, offset);
                        break;
                    case Expression::Sleepy:
                        DrawBubbleMark(spi, 290, 40, 10, primary_color, offset);
                        DrawBubbleMark(spi, 270, 52, 6, primary_color, -offset);
                    default:
                        // noop
                        break;
                }
            }

};

} // namespace

#endif // EFFECT_H_
