#ifndef EYEBLOW_H_
#define EYEBLOW_H_

#include <utility/In_eSPI.h>
#include "BoundingRect.h"
#include "DrawContext.h"
#include "Drawable.h"

namespace m5_test {

class Eyeblow final : public Drawable{
    private:
        uint16_t width_;
        uint16_t height_;
        bool is_left_;
    
    public:
        Eyeblow() = delete;
        Eyeblow(uint16_t w, uint16_t h, bool is_left);
        ~Eyeblow() = default;
        Eyeblow(const Eyeblow &other) = default;
        Eyeblow &operator=(const Eyeblow &other) = default;
        void draw(TFT_eSPI *spi, BoundingRect rect, DrawContext *draw_context) override;
};

}  // namespace m5_test

#endif // EYEBLOW_H_