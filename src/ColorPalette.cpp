#include "ColorPalette.h"

namespace m5_test {
ColorPalette::ColorPalette():
    colors_{{COLOR_PRIMARY, TFT_WHITE},
            {COLOR_SECONDARY, TFT_BLACK},
            {COLOR_BACKGROUND, TFT_BLACK}}
{
    // nothing
}

uint32_t ColorPalette::Get(const char* key)const{
    auto itr = colors_.find(key);
    if(itr != colors_.end()){
        return itr->second;
    }else{
        // NOTE: if no value it returns BLACK(0x00) as the default value of the
        // type(int)
        Serial.printf("no color with the key %s\n", key);
        return TFT_BLACK;
    }
}

void ColorPalette::Set(const char* key, uint32_t value){
    auto itr = colors_.find(key);
    if(itr != colors_.end()){
        Serial.println("Overwriting");
    }
    itr->second = value;

}


} // namaspace m5_test
