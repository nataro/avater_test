#ifndef GAZE_H_
#define GAZE_H_

namespace m5_test {

class Gaze{
    private:
        float vertical_;
        float horizonal_;

    public:
        Gaze();
        Gaze(float v, float h);
        ~Gaze() = default;
        Gaze(const Gaze &other) = default;
        
        float vertical() const{return vertical_;}
        float horizonal() const{return horizonal_;}
};


}  // namespace m5avatar

#endif // GAZE_H