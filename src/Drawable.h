#ifndef DRAWABLE_H_
#define DRAWABLE_H_


#include <utility/In_eSPI.h>
#include "BoundingRect.h"
#include "DrawContext.h"

namespace m5_test {
class Drawable{
    public:
        virtual ~Drawable() = default;
        virtual void draw(TFT_eSPI *spi, BoundingRect rect, DrawContext *draw_content) = 0;
        // virtual void draw(TFT_eSPI *spi, DrawContext *drawContext) = 0;
};


}
#endif // DRAWABLE_H_