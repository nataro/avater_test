#ifndef BALLOON_H_
#define BALLOON_H_


#include "DrawContext.h"
#include "Drawable.h"


const int16_t kTextHeight = 8;
const int16_t kTextSize = 2;
const int16_t kMinWidth = 40;
const uint32_t kPrimaryColor = TFT_BLACK;
const uint32_t kBackbreoudColor = TFT_WHITE;
const uint16_t kX = 240;
const uint16_t kY = 220;


namespace m5_test{
class Balloon final : public Drawable{
    public:
        Balloon() = default;
        ~Balloon() = default;
        Balloon(const Balloon &other) = default;
        Balloon &operator=(const Balloon &other) = default;

        void draw(TFT_eSPI *spi, BoundingRect rect, DrawContext *ctx) override;
};

} // m5_test

#endif // BALLOON_H_
