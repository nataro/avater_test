#include "BoundingRect.h"
#include <Arduino.h>

namespace m5_test{

BoundingRect::BoundingRect(int16_t top, int16_t left):
    BoundingRect(top, left, 0, 0)
{ // nothing 
}


BoundingRect::BoundingRect(int16_t top, int16_t left, int16_t width, int16_t height):
    top_(top),
    left_(left),
    width_(width),
    height_(height)
{// nothing 
}

int16_t BoundingRect::GetRight(){ return left_ + width_;}
int16_t BoundingRect::GetBottom(){ return top_ + height_;}
int16_t BoundingRect::GetCenterX(){ return left_ + width_ / 2 ;}
int16_t BoundingRect::GetCenterY(){ return top_ + height_ /2 ;} 

void BoundingRect::SetPosition(int16_t top, int16_t left){
    top_ = top;
    left_ = left;
}

void BoundingRect::SetSize(int16_t width, int16_t height){
    width_ = width;
    height_ = height;
}

} // namaspace m5_test
