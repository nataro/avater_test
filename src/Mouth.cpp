#include "Mouth.h"

namespace m5_test{

Mouth::Mouth(uint16_t min_width, uint16_t max_width, uint16_t min_height, uint16_t max_height) : 
    min_width_{min_width},
    max_width_{max_width},
    min_height_{min_height},
    max_height_{max_height}
{
    // nothing
}

void Mouth::draw(TFT_eSPI *spi, BoundingRect rect, DrawContext *ctx){
    uint32_t primary_color = ctx->palette()->Get(COLOR_PRIMARY);
    float_t breath = _min(1.0f, ctx->breath());
    float_t open_ratio = ctx->mouth_open_ratio();
    int16_t h = min_height_ + (max_width_ - min_height_) * open_ratio;
    int16_t w = min_width_ + (max_width_ - min_width_) * (1 - open_ratio);
    int16_t x = rect.left() - w / 2;
    int16_t y = rect.top() - h / 2 + breath * 2;
    spi->fillRect(x, y, w, h, primary_color);
}

} // namespace

