#ifndef MOUTH_H_
#define MOUTH_H_


#include <utility/In_eSPI.h>
#include "BoundingRect.h"
#include "DrawContext.h"
#include "Drawable.h"

namespace m5_test{

class Mouth final : public Drawable{
    private:
        uint16_t min_width_;
        uint16_t max_width_;
        uint16_t min_height_;
        uint16_t max_height_;

    public:
        Mouth() = delete;
        Mouth(
            uint16_t min_width,
            uint16_t max_width,
            uint16_t min_height,
            uint16_t max_height);
        ~Mouth() = default;
        Mouth(const Mouth &other) = default;
        Mouth &operator=(const Mouth &other) = default;

        void draw(TFT_eSPI * spi, BoundingRect rect, DrawContext * draw_context) override;
};

}  // namespace

#endif // MOUTH_H_