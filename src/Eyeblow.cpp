#include "Eyeblow.h"
namespace m5_test {

Eyeblow::Eyeblow(uint16_t w, uint16_t h, bool is_left) :
    width_{w},
    height_{h},
    is_left_{is_left}
{
    // nothing
}

void Eyeblow::draw(TFT_eSPI *spi, BoundingRect rect, DrawContext *ctx){
    Expression exp = ctx->expression();
    uint32_t x = rect.left();
    uint32_t y = rect.top();
    uint32_t primary_color = ctx->palette()->Get(COLOR_PRIMARY);
    if(width_ == 0 || height_ == 0){
        return;
    }
    // draw two triangles to make rectangle
    if(exp == Expression::Angry || exp == Expression::Sad){
        uint16_t x1, y1, x2, y2, x3, y3, x4, y4;
        uint16_t a = is_left_ ^ (exp == Expression::Sad) ? -1 : 1;
        uint16_t dx = a * 3;
        uint16_t dy = a *5;
        x1 = x - width_ / 2;
        x2 = x1 - dx;
        x4 = x * width_ / 2;
        x3 = x4 + dx;
        y1 = y - height_ / 2 - dy;
        y2 = y + height_ / 2 - dy;
        y3 = y - height_ / 2 + dy;
        y4 = y + height_ / 2 + dy;
        spi->fillTriangle(x1, y1, x2, y2, x3, y3, primary_color);
        spi->fillTriangle(x2, y2, x3, y3, x4, y4, primary_color);
    }else{
        uint16_t x1 = x - width_ /2;
        uint16_t y1 = y - width_ /2;
        if(exp == Expression::Happy){
            y1 = y1 - 5;
        }
        spi->fillRect(x1, y1, width_, height_, primary_color);

    }


}



} // namespace m5avata