#ifndef COLORPALETTE_H_
#define COLORPALETTE_H_

#include <M5Stack.h>
#include <string>
#include <map>

#define COLOR_PRIMARY "primary"
#define COLOR_SECONDARY "secondary"
#define COLOR_BACKGROUND "background"


namespace m5_test {

// enum class ColorType
// {
//   ONEBYTE,
//   TWOBYTE,
//   ONEBIT
// }


/**
 * Color palette for drawing face
 */ 
class ColorPalette{
    private:
        std::map<std::string, uint32_t> colors_;

    public:
        ColorPalette();
        ~ColorPalette() = default;
        ColorPalette(const ColorPalette &other) = default;
        ColorPalette &operator=(const ColorPalette &other) = default;

        uint32_t Get(const char *key) const;
        void Set(const char *key, uint32_t value);
        void Clear(void);


};

} // namaspace m5_test


#endif // COLORPALETTE_H_