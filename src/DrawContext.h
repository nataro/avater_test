
#ifndef DRAWCONTEXT_H_
#define DRAWCONTEXT_H_

#include "ColorPalette.h"
#include "Expression.h"
#include "Gaze.h"

namespace m5_test {
class DrawContext{
    private:
        Expression expression_;
        float_t breath_;
        float_t eye_open_ratio_;
        float_t mouth_open_ratio_;
        Gaze gaze_;
        ColorPalette * const palette_;
        const char* speech_text_;
        float_t rotation_ = 0.0;
        float_t scale_ =1.0;

    public:
        DrawContext() = delete;// 使用を制限
        
        DrawContext(Expression expression, 
                    float_t breath, 
                    ColorPalette* const palette, 
                    Gaze gaze, 
                    float_t eye_open_ratio, 
                    float_t mouth_open_ratio, 
                    const char* speech_text);

        DrawContext(Expression expression, 
                    float_t breath, 
                    ColorPalette* const palette, 
                    Gaze gaze, 
                    float_t eye_open_ratio, 
                    float_t mouth_open_ratio, 
                    const char* speech_text,
                    float_t rotation,
                    float_t scale);

        // DrawContext(Expression expression, float breath, ColorPalette* const palette,
        //       Gaze gaze, float eyeOpenRatio, float mouthOpenRatio,
        //       const char* speechText);
        // DrawContext(Expression expression, float breath, ColorPalette* const palette,
        //       Gaze gaze, float eye_open_ratio, float mouth_open_ratio,
        //         const char* speech_text, float rotation, float scale);

        ~DrawContext() = default;
        // 使用を制限
        DrawContext(const DrawContext& other) = delete;
        DrawContext& operator=(const DrawContext& other) = delete;

        Expression expression() const{return expression_;}
        float_t breath() const{return breath_;}
        float_t eye_open_ratio() const{return eye_open_ratio_;}
        float_t mouth_open_ratio() const{return mouth_open_ratio_;}
        float_t scale() const{return scale_;}
        float_t rotation() const{return rotation_;}
        Gaze gaze() const{return gaze_;}
        ColorPalette* const palette() const{return palette_;}
        const char* speech_text() const{return speech_text_;}

};


} // namaspace m5_test

#endif // DRAWCONTEXT_H_