#include <M5Stack.h>

#include "Avatar.h"

m5_test::Avatar avater;


// the setup routine runs once when M5Stack starts up
void setup(){

  Serial.begin(115200);



  // Initialize the M5Stack object
  M5.begin();

  // LCD display
  //M5.Lcd.print("Hello World test");


  // 動作確認用
  m5_test::Expression eps = m5_test::Expression::Angry;
  float breath = 1.0;
  float eye_open_ratio= 1.0;
  float mouth_open_ratio=1.0;
  m5_test::Gaze gaze(1,2);
  m5_test::ColorPalette *pale = new m5_test::ColorPalette();
  const char *text = "Tokyo";

  m5_test::DrawContext *ctx = new  m5_test::DrawContext(
    eps,
    breath,
    pale,
    gaze,
    eye_open_ratio,
    mouth_open_ratio,
    text
  );

  m5_test::Face face_test;
  face_test.draw(ctx);
  delete ctx;

  delay(1000);


  //実際はこれだけでいい
  avater.init();





}

// the loop routine runs over and over again forever
void loop() {

  delay(1000);
}