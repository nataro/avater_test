#include "Gaze.h"

namespace m5_test{

Gaze::Gaze():
    vertical_{0},
    horizonal_{0}
{
    //nothing
}

Gaze::Gaze(float v, float h):
    vertical_{h},
    horizonal_{v}
{
    //nothing
}

} // namespace m5_test