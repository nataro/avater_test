
#ifndef EXPRESSION_H_
#define EXPRESSION_H_

namespace m5_test {
enum class Expression { Happy, Angry, Sad, Doubt, Sleepy, Neutral };
} // namespace m5_test

#endif // EXPRESSION_H_