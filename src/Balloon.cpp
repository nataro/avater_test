#include "Balloon.h"
#include <M5Stack.h>


namespace m5_test{

void Balloon::draw(TFT_eSPI *spi, BoundingRect rect, DrawContext *ctx){
    const char *text = ctx->speech_text();
    if(strlen(text) == 0){
        return;
    }
    M5.Lcd.setTextSize(kTextSize);
    M5.Lcd.setTextDatum(MC_DATUM);
    spi->setTextSize(kTextSize);
    spi->setTextColor(kPrimaryColor, kBackbreoudColor);
    spi->setTextDatum(MC_DATUM);
    int16_t text_width = M5.Lcd.textWidth(text, 2);
    int16_t text_height = kTextHeight * kTextSize;
    spi->fillEllipse(kX, kY, _max(text_width, kMinWidth) + 2, text_height * 2 +2, kPrimaryColor);
    spi->fillTriangle(kX - 62, kY - 42, kX - 8, kY - 10, kX - 41, kY - 8, kPrimaryColor);
    spi->fillEllipse(kX, kY, _max(text_width, kMinWidth) + 2, text_height * 2 , kBackbreoudColor);
    spi->fillTriangle(kX - 60, kY - 40, kX - 10, kY - 10, kX - 40, kY - 10, kBackbreoudColor);
    spi->drawString(text, kX, kY, 2);
}

} // namespace