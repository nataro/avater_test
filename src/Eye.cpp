#include "Eye.h"
namespace m5_test {

Eye::Eye(uint16_t x, uint16_t y, uint16_t radius, bool is_left) : 
    Eye(radius, is_left)
{
    // nothing
}

Eye::Eye(uint16_t radius, bool is_left) : 
    radius_{radius},
    is_left_{is_left}
{
    // nothing
}


void Eye::draw(TFT_eSPI *spi, BoundingRect rect, DrawContext *ctx){
    Expression exp = ctx->expression();
    uint32_t x = rect.GetCenterX();
    uint32_t y = rect.GetCenterY();
    Gaze g = ctx->gaze();
    float open_ratio = ctx->eye_open_ratio();
    uint32_t offset_x = g.horizonal() * 3;
    uint32_t offset_y = g.vertical() *3;
    uint32_t primary_color = ctx->palette()->Get(COLOR_PRIMARY);
    uint32_t background_color = ctx->palette()->Get(COLOR_BACKGROUND);
    if(open_ratio > 0){
        spi->fillCircle(x + offset_x, y + offset_y, radius_, primary_color);
        // TODO(meganetaaan): Refactor
        if(exp == Expression::Angry || exp == Expression::Sad){
            int16_t x0, y0, x1, y1, x2, y2;
            x0 = x + offset_x - radius_;
            y0 = y + offset_y - radius_;
            x1 = x0 + radius_ * 2;
            y1 = y0;
            x2 = !is_left_ != !(exp == Expression::Sad) ? x0 : x1;
            y2 = y0 + radius_;
            spi->fillTriangle(x0, y0, x1, y1, x2, y2, background_color);
        }
        if(exp == Expression::Happy || exp == Expression::Sleepy){
            int16_t x0, y0, w, h;
            x0 = x + offset_x - radius_;
            y0 = y + offset_y - radius_;
            w = radius_ * 2 + 4;
            h = radius_ + 2;
            if(exp == Expression::Happy){
                y0 += radius_;
                spi->fillCircle(x + offset_x, y + offset_y, radius_ / 1.5, background_color);
            }
            spi->fillRect(x0, y0, w, h, background_color);
        }
    } else {
        int16_t x1 = x - radius_ + offset_x;
        int16_t y1 = y - radius_ + offset_y;
        int16_t w = radius_ * 2;
        int16_t h = 4;
        spi->fillRect(x1, y1, w, h, primary_color);
    }

}


} // namespace m5_test