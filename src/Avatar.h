#ifndef AVATAR_H_
#define AVATAR_H_
#include "ColorPalette.h"
#include "Face.h"

namespace m5_test {
class Avatar{

    private:
        // static Face face_buf;
        Face *face_;
        bool is_drawing_;
        Expression expression_;
        float_t breath_;
        float_t eye_open_ratio_;
        float_t mouth_open_ratio_;
        float_t gaze_vertical_;
        float_t gaze_horizontal_;
        float_t rotation_;
        float_t scale_;
        ColorPalette palette_;
        const char *speech_text_;

    public:
        Avatar();
        explicit Avatar(Face *face);// 引数を暗黙の型変換してうけとらいないようにexplicit
        // ↓メンバ変数にポインタがあるが、このクラス内でnewしないのでデストラクタを自分で書かなくてもいいのかな？
        ~Avatar() = default;
        Avatar(const Avatar &other) = default;
        Avatar &operator=(const Avatar &other) = default;

        void init(void);
        void SetGaze(float_t vertical, float_t horizonal);
        void draw(void);
        void start();
        void stop();
        void AddTask(TaskFunction_t f, const char* name);
        Face *GetFaceInstanceP();

        Face *face() const{return face_;}
        ColorPalette alette() const{return palette_;}
        bool is_drawing(){return is_drawing_;}
        void set_palette(ColorPalette cp){palette_ = cp;}
        void set_face(Face *face){face_ = face;}
        void set_breath(float_t f){breath_ = f;}
        void set_gaze_vertica(float_t f){gaze_vertical_ = f;}
        void set_gaze_horizonal(float_t f){gaze_horizontal_ = f;}
        void set_expression(Expression e){expression_ = e;}
        void set_eye_open_ratio(float_t f){eye_open_ratio_ = f;}
        void set_mouth_open_ratio(float_t f){mouth_open_ratio_ = f;}
        void set_speech_text(const char * txt){speech_text_ = txt;}
        void set_rotation(float_t f){rotation_ = f;}
        void set_scale(float_t f){scale_ = f;}

};

class DriveContext{
    private:
        // TODO(meganetaaan): cyclic reference
        Avatar *avatar_;

    public:
        DriveContext() = delete;
        explicit DriveContext(Avatar *avatar);
        ~DriveContext() = default;
        DriveContext(const DriveContext &other) = delete;
        DriveContext &operator=(const DriveContext &other) = delete;

        Avatar *avatar(){return avatar_;}
};


} // namaspace
#endif // AVATAR_H_