#ifndef BOUNDINGRECT_H_
#define BOUNDINGRECT_H_

#include <Arduino.h>

namespace m5_test {
class BoundingRect{
    private:
        int16_t top_;
        int16_t left_;
        int16_t width_;
        int16_t height_;

    public:
    BoundingRect() = default;
    ~BoundingRect() = default;
    BoundingRect(int16_t top, int16_t left);
    BoundingRect(int16_t top, int16_t left, int16_t width, int16_t height);
    BoundingRect(const BoundingRect &other) = default;
    BoundingRect &operator=(const BoundingRect &other) = default;
    int16_t GetRight();
    int16_t GetBottom();
    int16_t GetCenterX();
    int16_t GetCenterY();
    void SetPosition(int16_t top, int16_t left);
    void SetSize(int16_t width, int16_t height);


    inline int16_t top()const{return top_;}
    inline int16_t left()const{return left_;}
    inline int16_t wdth()const{return width_;}
    inline int16_t height()const{return height_;}

};
} // namaspace m5_test

#endif // BOUNDINGRECT_H_