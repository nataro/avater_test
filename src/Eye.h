#ifndef EYE_H_
#define EYE_H_

#include <utility/In_eSPI.h>
#include "DrawContext.h"
#include "Drawable.h"

namespace m5_test {

class Eye : public Drawable{
    private:
        uint16_t radius_;
        bool is_left_;
    public:
        // constructor
        Eye() = delete;
        Eye(uint16_t x, uint16_t y, uint16_t radius, bool is_left); //depercated
        Eye(uint16_t radius, bool is_left);
        ~Eye() = default;
        Eye &operator=(const Eye &other) = default;
        void draw(TFT_eSPI *spi, BoundingRect rect, DrawContext *draw_context) override;
};


}  // namespace m5_test

#endif // EYE_H_