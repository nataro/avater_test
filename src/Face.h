#ifndef FACE_H_
#define FACE_H_

#include "BoundingRect.h"
#include "Eye.h"
#include "Eyeblow.h"
#include "Mouth.h"

namespace m5_test {
class Face {
    private:
        Drawable *mouth_;
        BoundingRect *mouth_pos_;
        Drawable *eye_right_;
        BoundingRect *eye_right_pos_;
        Drawable *eye_left_;
        BoundingRect *eye_left_pos_;
        Drawable *eyeblow_right_;
        BoundingRect *eyeblow_right_pos_;
        Drawable *eyeblow_left_;
        BoundingRect *eyeblow_left_pos_;
        BoundingRect *bounding_rect_; // 描画範囲？
        TFT_eSprite *sprite_;
        TFT_eSprite *tmp_sprite_;

    public:
        Face();
        //Face(Drawable *eye_r, Drawable *eye_l, Drawable *eyeblow_r, Drawable *eyeblow_l);
        // // TODO(meganetaaan): apply builder pattern
        // Face(Drawable *mouth, BoundingRect *mouthPos, Drawable *eyeR,
        // BoundingRect *eyeRPos, Drawable *eyeL, BoundingRect *eyeLPos,
        // Drawable *eyeblowR, BoundingRect *eyeblowRPos, Drawable *eyeblowL,
        // BoundingRect *eyeblowLPos);
        Face(
            Drawable *mouth,
            BoundingRect *mouth_pos,
            Drawable *eye_r,
            BoundingRect *eye_r_pos, 
            Drawable *eye_l, 
            BoundingRect *eye_l_pos,
            Drawable *eyeblow_r, 
            BoundingRect *eyeblow_r_pos, 
            Drawable *eyeblow_l,
            BoundingRect *eyeblo_l_pos);


        ~Face(); // メンバ変数でポインタを扱うのでディストラクタを自分で書く？
        Face(const Face &other) = default;
        Face &operator=(const Face &other) = default;

        Drawable *mouth(){return mouth_;}
        Drawable *eye_left(){return eye_left_;}
        Drawable *eye_right(){return eye_right_;}
        BoundingRect *bounding_rect(){return bounding_rect_;}

        void set_mouth(Drawable *mouth){mouth_ = mouth;}
        void set_eye_left(Drawable *eye){eye_left_ = eye;}
        void set_eye_right(Drawable *eye){eye_right_ = eye;}
        // void set_eyeblow_left();
        // void set_eyeblow_right();

        void draw(DrawContext *ctx);


};



}  // namespace m5_test

#endif // FACE_H_