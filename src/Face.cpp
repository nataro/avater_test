#include "Face.h"
#include "Balloon.h"
#include "Effect.h"

#define PRIMARY_COLOE WHITE
#define SECONDARY_COLOR BLACK

// TODO: move to another file
void TransformSprite(TFT_eSprite * from, TFT_eSprite *to, float r, float s){
    uint16_t width = from->width();
    uint16_t height = from->height();
    uint16_t cx = width / 2;
    uint16_t cy = height / 2;
    float_t cos_r = cos(r);
    float_t sin_r =sin(r);

    for (int y2 = 0; y2 < height; y2++) {
        for (int x2 = 0; x2 < width; x2++) {
            int x1 = (((x2 - cx) * cos_r) - ((y2 - cy) * sin_r)) / s + cx;
            int y1 = (((x2 - cx) * sin_r) + ((y2 - cy) * cos_r)) / s + cy;
            if (x1 < 0 || x1 >= width || y1 < 0 || y1 >= height) {
                continue;
            }
            int color = from->readPixel(x1, y1);
            to->drawPixel(x2, y2, color);
        }
    }
}


namespace m5_test{
BoundingRect br; // BalloonやEffectに与えるためのダミーの変数 
Balloon balloon; // Balloonをグローバルに作っておく
Effect effect;


Face::Face()
    : Face(
        new Mouth(50, 90, 4, 60),
        new BoundingRect(148, 163),
        new Eye(8, false), 
        new BoundingRect(93, 90), 
        new Eye(8, true),
        new BoundingRect(96, 230), 
        new Eyeblow(32, 0, false),
        new BoundingRect(67, 96), 
        new Eyeblow(32, 0, true),
        new BoundingRect(72, 230))
{
    // nothing
}

Face::Face(
    Drawable *mouth,
    BoundingRect *mouth_pos, 
    Drawable *eye_r,
    BoundingRect *eye_r_pos, 
    Drawable *eye_l, 
    BoundingRect *eye_l_pos,
    Drawable *eyeblow_r, 
    BoundingRect *eyeblow_r_pos, 
    Drawable *eyeblow_l,
    BoundingRect *eyeblow_l_pos)
    : 
        mouth_{mouth},
        mouth_pos_{mouth_pos},
        eye_right_{eye_r},
        eye_right_pos_{eye_r_pos},
        eye_left_{eye_l},
        eye_left_pos_{eye_l_pos},
        eyeblow_right_{eyeblow_r},
        eyeblow_right_pos_{eyeblow_r_pos},
        eyeblow_left_{eyeblow_l},
        eyeblow_left_pos_{eyeblow_l_pos},
        bounding_rect_{new BoundingRect(0, 0, 320, 240)},//描画範囲？
        sprite_{new TFT_eSprite(&M5.Lcd)},
        tmp_sprite_{new TFT_eSprite(&M5.Lcd)}
{
    //nothng
}

Face::~Face(){
    delete mouth_;
    delete mouth_pos_;
    delete eye_right_;
    delete eye_right_pos_;
    delete eye_left_;
    delete eye_left_pos_;
    delete eyeblow_right_;
    delete eyeblow_right_pos_;
    delete eyeblow_left_;
    delete eyeblow_left_pos_;
    delete sprite_;
    delete tmp_sprite_;
    delete bounding_rect_;
}

void Face::draw(DrawContext *ctx){
    sprite_->setColorDepth(8);
    // NOTE: setting below for 1-bit color depth
    sprite_->setBitmapColor(ctx->palette()->Get(COLOR_PRIMARY), ctx->palette()->Get(COLOR_BACKGROUND));
    sprite_->createSprite(320, 240);
    sprite_->fillSprite(ctx->palette()->Get(COLOR_BACKGROUND));
    float_t breath = _min(1.0f, ctx->breath());

    // // TODO(meganetaaan): unify drawing process of each parts
    BoundingRect rect = *mouth_pos_;
    rect.SetPosition(rect.top() + breath * 3, rect.left());
    // copy context to each draw function
    mouth_->draw(sprite_, rect, ctx);

    rect = *eye_right_pos_;
    rect.SetPosition(rect.top() + breath * 3, rect.left());
    eye_right_->draw(sprite_, rect, ctx);

    rect = *eye_left_pos_;
    rect.SetPosition(rect.top() + breath * 3, rect.left());
    eye_left_->draw(sprite_, rect, ctx);

    rect = *eyeblow_right_pos_;
    rect.SetPosition(rect.top() + breath * 3, rect.left());
    eyeblow_right_->draw(sprite_, rect, ctx);

    rect = *eyeblow_left_pos_;
    rect.SetPosition(rect.top() + breath * 3, rect.left());
    eyeblow_left_->draw(sprite_, rect, ctx);

    //   // TODO(meganetaaan): make balloons and effects selectable
    balloon.draw(sprite_, br, ctx);
    effect.draw(sprite_, br, ctx);
    // // drawAccessory(sprite, position, ctx);

    // TODO(meganetaaan): rethink responsibility for transform function
    float_t scale = ctx->scale();
    float_t rotation = ctx->rotation();

    if(scale != 1.0 || rotation != 0){
        // TODO(meganetaaan): reduce memory usage, at least small(1-bit) color depth
        tmp_sprite_->setColorDepth(8);
        tmp_sprite_->setBitmapColor(ctx->palette()->Get(COLOR_PRIMARY), ctx->palette()->Get(COLOR_BACKGROUND));
        tmp_sprite_->createSprite(320, 240);
        tmp_sprite_->fillSprite(ctx->palette()->Get(COLOR_BACKGROUND));
        TransformSprite(sprite_, tmp_sprite_, rotation, scale);

        tmp_sprite_->pushSprite(bounding_rect_->left(), bounding_rect_->top());
        tmp_sprite_->deleteSprite();
    }else{
        sprite_->pushSprite(bounding_rect_->left(), bounding_rect_->top());
    }
    sprite_->deleteSprite();
}

} // namespace m5_test



