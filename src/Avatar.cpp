#include "Avatar.h"
// #include <random>


namespace m5_test{

const uint32_t DEFAULT_STACK_SIZE = 4096;

//メルセンヌツイスタ
// std::mt19937 mt;

DriveContext::DriveContext(Avatar *avatar):
    avatar_{avatar}
{
    //nothing
}

void UpdateBreath(void *args){
    int16_t c = 0;
    DriveContext *ctx = reinterpret_cast<DriveContext *>(args);
    for(;;){
        c = c + 1 % 100;
        float_t f = sin(c * 2 * PI / 100.0);
        ctx->avatar()->set_breath(f);
        delay(33);
        //Serial.println(" update breath ");
    }
}

void DrawLoop(void *args){
    DriveContext *ctx = reinterpret_cast<DriveContext *>(args);
    Avatar *avatar = ctx->avatar();
    for(;;){
        if(avatar->is_drawing()){
            avatar->draw();
        }
        //delay(33);
    }
}

void Saccade(void *args) {
  DriveContext *ctx = reinterpret_cast<DriveContext *>(args);
  for (;;) {
    float_t vertical = rand() / (RAND_MAX / 2.0) - 1;
    // float_t vertical = mt() / (mt.max() / 2.0) - 1;
    float_t horizontal = rand() / (RAND_MAX / 2.0) - 1;
    // float_t horizontal = mt() / (mt.max() / 2.0) - 1;
    ctx->avatar()->SetGaze(vertical, horizontal);
    delay(500 + 100 * random(20));
  }
}

void Blink(void *args) {
  DriveContext *ctx = reinterpret_cast<DriveContext *>(args);
  for (;;) {
    ctx->avatar()->set_eye_open_ratio(1);
    delay(2500 + 100 * random(20));
    ctx->avatar()->set_eye_open_ratio(0);
    delay(300 + 10 * random(20));
  }
}

Avatar::Avatar() : 
    Avatar(GetFaceInstanceP())
{
    //nothing
}

Avatar::Avatar(Face *face) :
    face_{face},
    is_drawing_{true},
    // expression_{Expression::Neutral},
    expression_{Expression::Happy},
    breath_{0},
    eye_open_ratio_{1},
    mouth_open_ratio_{0},
    gaze_vertical_{0},
    gaze_horizontal_{0},
    rotation_{0},
    scale_{1},
    palette_{ColorPalette()},
    speech_text_{""}
    // speech_text_{"Avater(Face *) ni kinyu sita moji"}
{
    // nothing
}

Face *Avatar::GetFaceInstanceP(){
    // Avatar()のコンストラクタにおいてFace  が指定されていないときは
    // クラス内のスタティックなインスタンスのアドレスを返して使いたい
    static Face instance;
    return &instance;
}


void Avatar::AddTask(TaskFunction_t f, const char* name) {
    DriveContext *ctx = new DriveContext(this);
    // TODO(meganetaaan): set a task handler
    xTaskCreate( // xTaskCreate は１番目のコアのみ動作する関数?
        f,                  /* Function to implement the task */
        name,               /* Name of the task */
        DEFAULT_STACK_SIZE, /* Stack size in words */
        ctx,                /* Task input parameter */
        1,                  /* P2014riority of the task */
        NULL);              /* Task handle. */
}

void Avatar::SetGaze(float_t vertical, float_t horizonal){
    gaze_vertical_= vertical;
    gaze_horizontal_ = horizonal;
}

void Avatar::init() {
    DriveContext *ctx = new DriveContext(this);

    // TODO(meganetaaan): keep handle of these tasks
    xTaskCreate(DrawLoop,     /* Function to implement the task */
                "drawLoop",   /* Name of the task */
                DEFAULT_STACK_SIZE,         /* Stack size in words */
                ctx,          /* Task input parameter */
                1,            /* Priority of the task */
                NULL);        /* Task handle. */
    xTaskCreate(Saccade,      /* Function to implement the task */
                "saccade",    /* Name of the task */
                DEFAULT_STACK_SIZE,         /* Stack size in words */
                ctx,          /* Task input parameter */
                3,            /* Priority of the task */
                NULL);        /* Task handle. */
    xTaskCreate(UpdateBreath, /* Function to implement the task */
                "breath",     /* Name of the task */
                DEFAULT_STACK_SIZE,         /* Stack size in words */
                ctx,          /* Task input parameter */
                2,            /* Priority of the task */
                NULL);        /* Task handle. */
    xTaskCreate(Blink,        /* Function to implement the task */
                "blink",      /* Name of the task */
                DEFAULT_STACK_SIZE,         /* Stack size in words */
                ctx,          /* Task input parameter */
                2,            /* Priority of the task */
                NULL); /* Task handle. */
}

void Avatar::stop() {
    is_drawing_ = false;
}

void Avatar::start() {
    is_drawing_ = true;
}

void Avatar::draw(){
    Gaze g = Gaze(gaze_vertical_, gaze_horizontal_);
    DrawContext *ctx = new DrawContext(
        expression_,
        breath_,
        &palette_,
        g,
        eye_open_ratio_,
        mouth_open_ratio_,
        speech_text_,
        rotation_,
        scale_
    );


    face_->draw(ctx);

    delete ctx; // new したものをすぐにdeleteしている
}

} // namespace



