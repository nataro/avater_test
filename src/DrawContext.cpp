
#include "DrawContext.h"

namespace m5_test {

DrawContext::DrawContext(Expression expression, 
                    float_t breath, 
                    ColorPalette* const palette, 
                    Gaze gaze, 
                    float_t eye_open_ratio, 
                    float_t mouth_open_ratio, 
                    const char* speech_text):
    DrawContext(expression, breath, palette, gaze, eye_open_ratio, mouth_open_ratio, speech_text, 0, 1)
{
    // nothing
}



DrawContext::DrawContext(Expression expression, 
                    float_t breath, 
                    ColorPalette* const palette, 
                    Gaze gaze, 
                    float_t eye_open_ratio, 
                    float_t mouth_open_ratio, 
                    const char* speech_text,
                    float_t rotation,
                    float_t scale):
    expression_{expression},
    breath_{breath},
    eye_open_ratio_{eye_open_ratio},
    mouth_open_ratio_{mouth_open_ratio},
    gaze_{gaze},
    palette_{palette},
    speech_text_{speech_text},
    rotation_{rotation},
    scale_{scale}
{
    // nothing
}


}

